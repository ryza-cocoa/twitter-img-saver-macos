# twitter-img-saver-macos

![screenshot1](macos-twitter-img-saver.png)

![screenshot2](macos-twitter-img-saver-menubar.png)

### Compile
```bash
git clone --recursive https://magic.ryza.moe/ryza/twitter-img-saver-macos.git
cd twitter-img-saver-macos
xcodebuild -scheme TwitterImgSaver -configuration Release -derivedDataPath release
# check for TwitterImgSaver.app in release/Build/Products/Release
```

### Setup
Click the `Settings` menu, fill in the consumer key and secret, and then select the directory where you'd like to save the images. Finally, add some accounts (without @) you interested in to the list! 

Total number and total size of images saved for current session will be displayed in the drop-down menu.
