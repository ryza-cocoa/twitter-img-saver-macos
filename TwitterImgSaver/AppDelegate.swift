//
//  AppDelegate.swift
//  TwitterImgSaver
//
//  Created by Ryza on 27/10/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

import Cocoa
import SwifterMac

@NSApplicationMain
class AppDelegate: NSWindowController, NSApplicationDelegate {
    var records: Dictionary<String, Dictionary<String, Bool>>
    var timer: Timer?
    var num_images_downloaded: UInt64
    var images_size: UInt64
    var mtx: NSLock
    var staticticsMenu: NSMenuItem?
    
    var statusBarItem: NSStatusItem!
    var imageSaveDirectory: URL?
    let settingsWindow: NSWindowController = {
        // lazy prepare and display the second clock window, London
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let settingsWindow = storyboard.instantiateController(withIdentifier: "Settings") as! NSWindowController
        return settingsWindow
    }()
    
    override init(window: NSWindow?) {
        self.records = Dictionary.init()
        self.num_images_downloaded = 0
        self.images_size = 0
        self.mtx = NSLock.init()
        super.init(window: window)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateStatictics() {
        if let staticticsMenu = self.staticticsMenu {
            staticticsMenu.title = self.getStaticticsString()
        }
    }
    
    private func fetch_newest_images() {
        guard let key = AppDelegate.consumerKey() else { return }
        guard let secret = AppDelegate.consumerSecret() else { return }
        let swifter = Swifter(consumerKey: key, consumerSecret: secret)
        let accounts = AppDelegate.loadTwitterAccountList()
        let imageDirectoryPath = AppDelegate.loadImageSaveDirectory()?.path
        guard let imageDirectory = imageDirectoryPath else { return }
        
        accounts.forEach { username in
            self.records[username] = Dictionary.init();
        }
        accounts.forEach { username in
            swifter.getTimeline(for: UserTag.screenName(username), count: 50, excludeReplies: true, includeRetweets: false, success: { tweet in
                guard let tweets = tweet.array else { return }
                tweets.forEach {
                    guard let media = $0["extended_entities"]["media"].array else { return }
                    media.filter {
                        guard let media_type = $0["type"].string else { return false }
                        return media_type == "photo"
                    }.forEach { image -> Void in
                        guard let imageID = image["id_str"].string else { return }
                        guard var media_url_https = image["media_url_https"].string else { return }
                        media_url_https += "?format=png&name=large"
                        if self.records[username]![imageID] ?? false == false {
                            let imageDir = String.init(format: "%@/%@", imageDirectory, username)
                            let imagePath = String.init(format: "%@/%@.png", imageDir, imageID)
                            let imageDirURL = URL.init(fileURLWithPath: imageDir, isDirectory: true)
                            let imagePathURL = URL.init(fileURLWithPath: imagePath, isDirectory: false)
                            let fileManager = FileManager.init()
                            
                            do {
                                try fileManager.createDirectory(at: imageDirURL, withIntermediateDirectories: true, attributes: nil)
                                self.records[username]![media_url_https] = fileManager.fileExists(atPath: imagePath)
                                if !self.records[username]![media_url_https]! {
                                    guard let download_url = URL.init(string: media_url_https) else { return }
                                    var req = URLRequest.init(url:download_url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30.0)
                                    req.httpMethod = "GET"
                                    DispatchQueue.global(qos: .default).async {
                                        let task = URLSession.shared.dataTask(with: req, completionHandler: { (data, resp, err) in
                                            if err == nil {
                                                do {
                                                    try data!.write(to: imagePathURL)
                                                    self.mtx.lock()
                                                    self.num_images_downloaded += 1
                                                    self.images_size += UInt64(data!.count)
                                                    self.updateStatictics()
                                                    self.mtx.unlock()
                                                } catch {
                                                    print("[ERROR] Unexpected error: \(error.localizedDescription).")
                                                }
                                                self.records[username]![media_url_https] = true
                                            } else {
                                                print("[ERROR] image at url \(download_url): \(err.debugDescription)")
                                            }
                                        })
                                        task.resume()
                                    }
                                }
                            } catch {
                                print("[ERROR] Unexpected error: \(error.localizedDescription).")
                            }
                        }
                    }
                }
            })
        }
    }
    
    /// Show settings window in front
    @objc func showSettings() {
        settingsWindow.showWindow(self)
        settingsWindow.window?.orderFrontRegardless()
    }
    
    /// Quit this application
    @objc func quit() {
        NSApplication.shared.terminate(self)
    }
    
    private func getStaticticsString() -> String {
        var downloadedSize = Double(self.images_size)
        var sizeUnit = "MB"
        if downloadedSize < 1024 * 1024 {
            downloadedSize = downloadedSize / 1024.0 / 1024
        } else if self.images_size < 1024 * 1024 * 1024 {
            downloadedSize = downloadedSize / 1024.0 / 1024 / 1024
            sizeUnit = "GB"
        } else if self.images_size < 1024 * 1024 * 1024 * 1024 {
            downloadedSize = downloadedSize / 1024.0 / 1024 / 1024 / 1024
            sizeUnit = "TB"
        } else if self.images_size < 1024 * 1024 * 1024 * 1024 * 1024 {
            downloadedSize = downloadedSize / 1024.0 / 1024 / 1024 / 1024 / 1024
            sizeUnit = "EB"
        }
        return String.init(format: "%d images, %.2f %@", self.num_images_downloaded, downloadedSize, sizeUnit)
    }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Status Bar
        let statusMenu = NSMenu(title: "Twitter Image Saver")
        
        let configMenuItem = NSMenuItem(title: "Settings", action: #selector(AppDelegate.showSettings), keyEquivalent: "")
        self.staticticsMenu = NSMenuItem(title: self.getStaticticsString(), action: nil, keyEquivalent: "")
        self.staticticsMenu!.isEnabled = false
        
        let quitMenuItem = NSMenuItem(title: "Quit", action: #selector(AppDelegate.quit), keyEquivalent: "")
        quitMenuItem.keyEquivalentModifierMask = .command
        quitMenuItem.keyEquivalent = "Q"
        
        statusMenu.addItem(configMenuItem)
        statusMenu.addItem(self.staticticsMenu!)
        statusMenu.addItem(quitMenuItem)
        statusBarItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
        statusBarItem.menu = statusMenu
        statusBarItem.button?.image = NSImage(named: "Icon")
        statusBarItem.button?.imageScaling = .scaleProportionallyUpOrDown
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 600, repeats: true, block: { (Timer) in
            self.fetch_newest_images()
        })
        self.timer?.fireDate = Date.distantPast
        self.timer?.fire()
    }
    
    public static func consumerKey() -> String? {
        return UserDefaults.standard.string(forKey: "key")
    }
    
    public static func consumerSecret() -> String? {
        return UserDefaults.standard.string(forKey: "secret")
    }
    
    @objc public static func updateTwitterConsumerKey(sender: NSTextField!) {
        let key = sender.stringValue
        if key.lengthOfBytes(using: .utf8) > 0 {
            UserDefaults.standard.set(key, forKey: "key")
        }
    }
    
    @objc public static func updateTwitterConsumerSecret(sender: NSTextField!) {
        let secret = sender.stringValue
        if secret.lengthOfBytes(using: .utf8) > 0 {
            UserDefaults.standard.set(secret, forKey: "secret")
        }
    }
    
    public static func updateImageSaveDirectory(dir: URL?) {
        guard let url = dir else {
            return
        }
        UserDefaults.standard.set(url, forKey: "img_dir")
    }
    
    public static func loadImageSaveDirectory() -> URL! {
        guard let url = UserDefaults.standard.url(forKey: "img_dir") else {
            return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        }
        return url
    }
    
    private static func filterAccountList(accountList: [String]) -> [String] {
        return accountList.filter { account in
            let account = account.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let regex = try! NSRegularExpression(pattern: "[a-zA-Z0-9_]+")
            let range = NSRange(location: 0, length: account.utf8.count)
            let match = regex.firstMatch(in: account, options: [], range: range)
            guard let m = match else { return false }
            guard m.range.length == account.utf8.count else { return false }
            return true
        }
    }
    
    public static func updateTwitterAccountList(accountList: [String]) {
        let accountList = filterAccountList(accountList: accountList)
        UserDefaults.standard.set(accountList, forKey: "twitter_account")
    }
    
    public static func loadTwitterAccountList() -> [String] {
        var accountList: [String] = []
        let arr = UserDefaults.standard.array(forKey: "twitter_account")
        if arr == nil || arr?.count == 0 {
            return accountList
        } else {
            arr?.forEach({ ele in
                if let account = ele as? String {
                    accountList.append(account)
                }
            })
            return filterAccountList(accountList: accountList)
        }
    }
}
