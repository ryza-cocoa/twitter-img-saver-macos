//
//  SettingsWindow.swift
//  TwitterImgSaver
//
//  Created by Ryza on 28/10/2020.
//  Copyright © 2020 Ryza. All rights reserved.
//

import Foundation
import Cocoa

class SettingsWindow: NSWindow {
    override init(contentRect: NSRect, styleMask style: NSWindow.StyleMask, backing backingStoreType: NSWindow.BackingStoreType, defer flag: Bool) {
        super.init(contentRect: contentRect, styleMask: style, backing: backingStoreType, defer: flag)
    }
}

class SettingsView: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    @IBOutlet weak var chooseImageSaveDirectoryButton: NSButton!
    @IBOutlet weak var showImageSaveDirectoryButton: NSButton!
    @IBOutlet weak var imageSaveDirectoryTextField: NSTextField!
    @IBOutlet weak var twitterConsumerKeyTextField: NSTextField!
    @IBOutlet weak var twitterConsumerSecretTextField: NSTextField!
    @IBOutlet weak var twitterAccountTable: NSTableView!
    @IBOutlet weak var addTwitterAccountButton: NSButton!
    @IBOutlet weak var removeSelectedTwitterAccountButton: NSButton!
    
    var accountList: [String]
    
    required init?(coder: NSCoder) {
        self.accountList = []
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        self.chooseImageSaveDirectoryButton.action = #selector(SettingsView.chooseImageSaveDirectory)
        self.showImageSaveDirectoryButton.action = #selector(SettingsView.showImageSaveDirectory)
        self.imageSaveDirectoryTextField.isEditable = false
        self.twitterConsumerKeyTextField.action = #selector(SettingsView.updateTwitterConsumerKey)
        self.twitterConsumerKeyTextField.target = self
        self.twitterConsumerSecretTextField.action = #selector(SettingsView.updateTwitterConsumerSecret)
        self.twitterConsumerSecretTextField.target = self
        self.twitterAccountTable.delegate = self
        self.twitterAccountTable.dataSource = self
    }
    
    override func viewWillAppear() {
        self.twitterConsumerKeyTextField.stringValue = AppDelegate.consumerKey() ?? ""
        self.twitterConsumerSecretTextField.stringValue = AppDelegate.consumerSecret() ?? ""
        reloadTwitterAccountList(fromAppDelegate: true)
        displayCurrentImageSaveDirectory()
    }
    
    override func viewWillDisappear() {
        AppDelegate.updateTwitterConsumerKey(sender: self.twitterConsumerKeyTextField)
        AppDelegate.updateTwitterConsumerSecret(sender: self.twitterConsumerSecretTextField)
        AppDelegate.updateTwitterAccountList(accountList: self.accountList)
    }
    
    private func displayCurrentImageSaveDirectory() {
        self.imageSaveDirectoryTextField.stringValue = AppDelegate.loadImageSaveDirectory()?.path ?? ""
    }
    
    /// Number of rows in clock table
    /// - Parameter tableView: tableView
    func numberOfRows(in tableView: NSTableView) -> Int {
        return self.accountList.count;
    }
    
    /// Return corrresponding row cell
    /// - Parameters:
    ///   - tableView: which table view is asking for data
    ///   - tableColumn: which column it acquires
    ///   - row: which row it wants
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        // since we only have one table view here
        // so we just skip some checks
        let account = self.accountList[row]
        let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView
        cell?.textField?.stringValue = account
        return cell
    }
    
    /// Reload Twitter account list
    func reloadTwitterAccountList(fromAppDelegate reload: Bool = true) {
        if reload {
            self.accountList = AppDelegate.loadTwitterAccountList()
        }
        
        self.twitterAccountTable.reloadData()
    }
    
    /// Add new account
    /// - Parameter sender: Add button
    @IBAction func add(sender: Any) {
        self.accountList.append("#new account")
        reloadTwitterAccountList(fromAppDelegate: false)
    }
    
    /// Remove current selected account
    /// - Parameter sender: Remove button
    @IBAction func remove(sender: Any) {
        let selectedRow = self.twitterAccountTable.selectedRow
        guard selectedRow > -1 && selectedRow < self.accountList.count else {
            return
        }
        self.accountList.remove(at: selectedRow)
        reloadTwitterAccountList(fromAppDelegate: false)
    }
    
    @IBAction func tableViewDoubleClick(sender: NSTableView) {
        let column = sender.clickedColumn
        let row = sender.clickedRow

        // Abort when clicked outside the table bounds
        guard column > -1 else { return }

        if row == -1 {
            return
        }

        editCell(tableView: sender, column: column, row: row)
    }
    
    private func editCell(tableView: NSTableView, column: Int, row: Int) {
        guard row > -1 && column > -1 else { return }
        guard let view = tableView.view(atColumn: column, row: row, makeIfNecessary: true) as? NSTableCellView
            else { return }

        view.textField?.selectText(self)
        view.textField?.action = #selector(SettingsView.updateAccount)
        view.textField?.target = self
        view.textField?.tag = row
    }
    
    @objc func updateAccount(sender: NSTextField!) {
        let row = sender.tag
        guard row > -1 && row < self.accountList.count else {
            return
        }
        
        self.accountList[row] = sender.stringValue
    }
    
    @objc func updateTwitterConsumerKey(sender: NSTextField!) {
        AppDelegate.updateTwitterConsumerKey(sender: self.twitterConsumerKeyTextField)
    }
    
    @objc func updateTwitterConsumerSecret(sender: NSTextField!) {
        AppDelegate.updateTwitterConsumerSecret(sender: self.twitterConsumerSecretTextField)
    }
    
    @objc func chooseImageSaveDirectory(sender: Any) {
        let openPanel = NSOpenPanel()
        openPanel.canChooseDirectories = true
        openPanel.canChooseFiles = false
        openPanel.allowsMultipleSelection = false
        openPanel.resolvesAliases = true
        openPanel.message = "Choose the directory for saving images"
        openPanel.prompt = "Choose"
        openPanel.allowedFileTypes = ["none"]
        openPanel.allowsOtherFileTypes = false
        openPanel.begin { (result) -> Void in
            if result == .OK {
                AppDelegate.updateImageSaveDirectory(dir: openPanel.url!)
            } else {
                AppDelegate.updateImageSaveDirectory(dir: nil)
            }
            self.displayCurrentImageSaveDirectory()
        }
    }
    
    @objc func showImageSaveDirectory(sender: Any) {
        NSWorkspace.shared.activateFileViewerSelecting([AppDelegate.loadImageSaveDirectory()])
    }
}
